# Задача 1: Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.

def get_float(char):

    # Специально сделал float чтобы функция всегда возвращала результат одного типа
    result_default = 0.0

    try:
        result = float(char)
    except ValueError:
        result = result_default
    except Exception:
        result = result_default

    return result


user_char = input("Введите число: ").strip()
user_float = get_float(user_char)
print(f"Число : {user_float} Тип: {type(user_float)}")

# Задача 2: Напишіть функцію, що приймає два аргументи. Функція повинна
# а. якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
# б. якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
# в. у будь-якому іншому випадку повернути кортеж з цих аргументів


def get_result(arg1, arg2):

    type_arg1 = type(arg1)
    type_arg2 = type(arg2)

    if (type_arg1 == int or type_arg1 == float) and (type_arg2 == int or type_arg2 == float):
        result = arg1 * arg2
    elif type_arg1 == str and type_arg2 == str:
        result = arg1 + arg2
    else:
        result = list()
        result.append(arg1)
        result.append(arg2)

    return result


def print_result(arg1, arg2):

    result = get_result(arg1, arg2)
    print(f"arg1 = {arg1} arg2 = {arg2} result = {result}")


print_result(2, 12.1212)
print_result("Хлеб", "Масло")
print_result("12", None)

# Задача 3: Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
# Попросіть користувача ввести свій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
# Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік.
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку
# користувача (1 - рік, 22 - роки, 35 - років і тд...).
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "Незважаючи на те, що вам 42 роки, білетів всеодно нема!"


def get_age():

    while True:

        age_str = input("Укажите свой возраст: ").strip(" +-")
        if not age_str.isdigit():

            print("Введены некорректные данные! Допускаются только целые числа.")

        else:

            break

    result = int(age_str)

    return result


def get_agestr(age):

    remainder = age % 10 if age > 20 else age

    if remainder == 1:
        result = "год"
    elif 2 <= remainder <= 4:
        result = "года"
    else:
        result = "лет"

    return result


def print_ticket_answer(age):

    happy_char = "7"
    full_age = f"{age} {get_agestr(age)}"

    if happy_char in str(age):
        print(f"Вам {full_age}, вам повезет!")
    elif age < 7:
        print(f"Тебе ж {full_age}! Где твои родители?")
    elif age < 16:
        print(f"Тебе всего {full_age}, а это фильм для взрослых!")
    elif age > 65:
        print(f"Вам {full_age}! Покажите пенсионное удовстоверение!")
    else:
        print(f"Несмотря на то, что вам {full_age}, билетов всё равно нет!")


user_age = get_age()
print_ticket_answer(user_age)
