# Задача 1: Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель",
# наслідувані від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд.

class Vehicles:

    brand = None
    model = None

    def __init__(self, new_brand, new_model):

        self.brand = new_brand
        self.model = new_model

    def get_info(self):

        print(f"brand: {self.brand} model: {self.model}")


class Cars(Vehicles):

    number_of_seats = 18


class Airplanes(Vehicles):

    speed = 600


class Ships(Vehicles):

    load_capacity = 10000


# Задача 2: Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".

car1 = Cars("mazda", "а30")
airplan1 = Airplanes("boing", "su30")
ship1 = Ships("titaniс", "1")

car1.get_info()
airplan1.get_info()
ship1.get_info()
