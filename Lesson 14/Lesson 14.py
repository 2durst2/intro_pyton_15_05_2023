
# Задача 1: Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ),
# отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
# "[дата, на яку актуальний курс]"
# 1. [назва валюти 1] to UAH: [значення курсу валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу валюти 2]
# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс

import requests


class BankNBU:

    url = "https://bank.gov.ua"

    @staticmethod
    def _get_date():

        while True:
            user_date = input("Введите дату в форме ггггммдд (Например: 20230704) :").strip()
            if user_date.isdigit() and len(user_date) == 8:
                return user_date
            else:
                print("Введены некорректные данные")

    @staticmethod
    def _user_answer():

        while True:

            user_input = input("Записать курс валют на текущую дату? Введите Да/Нет: ").lower().strip()
            if user_input == "да":
                return True
            elif user_input == "нет":
                return False
            else:
                print("Введены некорректные данные")

    def _get_new_url(self):

        new_url = f"{self.url}/NBUStatService/v1/statdirectory/exchange?"
        user_answer = self._user_answer()
        if user_answer is True:

            new_url = f"{new_url}json"

        else:
            date = self._get_date()
            new_url = f"{new_url}date={date}&json"

        return new_url

    def _write_rates(self, new_url):

        try:
            res = requests.request("GET", new_url, timeout=3)
        except:
            print(f"Ошибка, не удалось подключиться к {new_url}")
        else:
            if 200 <= res.status_code < 300:

                list_content = res.json()
                with open("exchange rates.txt", "w") as file:

                    for l_dict in list_content:

                        currency = l_dict.get("txt")
                        rates = l_dict.get("rate")
                        file.write(f"{currency} до грн: {rates}\n")
            else:
                print("Не удалось загрузить курсы валют")

    def get_rates(self):

        new_url = self._get_new_url()
        self._write_rates(new_url)


bank1 = BankNBU()
bank1.get_rates()
