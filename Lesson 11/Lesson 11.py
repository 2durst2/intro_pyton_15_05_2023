
# Задача 1: Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати
# тільки обʼєкти класу int або float


class Point:
    x_coord = 0
    y_coord = 0

    def __init__(self, x, y):

        self.x_coord = x
        self.y_coord = y

    def __str__(self):

        return f'Point {self.x_coord} {self.y_coord}'

    def __getitem__(self, item):

        # print(f'__getitem__ {item}')
        if item == 0:
            return self.x_coord
        elif item == 1:
            return self.y_coord
        else:
            raise TypeError

    def __setitem__(self, item, value):
        # print(f'__setitem__ {item}, {value}')
        if item == 0:
            self.x_coord = value
        elif item == 1:
            self.y_coord = value
        else:
            raise TypeError

    def __eq__(self, other):
        # check type Point
        return self.x_coord == other.x_coord and self.y_coord == other.y_coord

    def __setattr__(self, key, value):

        type_value = type(value)
        if type_value != int and type_value != float:
            raise TypeError(f"for key {key} value must bee int or float")

        self.__dict__[key] = value

# Задача 2: Доопрацюйте класс Line так, щоб в атрибути begin та end обʼєктів цього класу можна було
# записати тільки обʼєкти класу Point


class Line:

    begin_point = None
    end_point = None

    def __init__(self, begin, end):

        self.begin_point = begin
        self.end_point = end

    def __str__(self):
        return f'Line from [{self.begin_point}] to [{self.end_point}]'

    def length(self):
        k1 = self.begin_point.x_coord - self.end_point.x_coord
        k2 = self.begin_point.y_coord - self.end_point.y_coord

        return (k1 ** 2 + k2 ** 2) ** 0.5

    def __len__(self):
        """ len(obj) """
        return 2

    def __contains__(self, item):
        """ a in b """
        print('__contains__', item)
        return self.begin_point == item or self.end_point == item

    def __setattr__(self, key, value):

        list_key = ["begin_point", "end_point"]
        if key in list_key and not isinstance(value, Point):
            raise TypeError(f"for key: {key} value must bee class Point")

        self.__dict__[key] = value


# Задача 3: Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, аналогічно до класу Line.
# Визначет метод, що містить площу трикутника. Для обчислень можна використати формулу Герона


class Triangle:

    point_a = None
    point_b = None
    point_c = None

    def __init__(self, new_a, new_b, new_c):

        self.point_a = new_a
        self.point_b = new_b
        self.point_c = new_c

    def __setattr__(self, key, value):

        list_key = ["point_a", "point_b", "point_c"]  # Возможно будут другие атрибуты которые проверять не нужно
        if key in list_key and not isinstance(value, Point):
            raise TypeError(f"for key: {key} value must bee class Point")

        self.__dict__[key] = value

    def square(self):

        line_a = Line(self.point_a, self.point_b)
        line_b = Line(self.point_b, self.point_c)
        line_c = Line(self.point_a, self.point_c)

        length_a = line_a.length()
        length_b = line_b.length()
        length_c = line_c.length()

        semi_perimeter = 0.5 * (length_a + length_b + length_c)
        result = (semi_perimeter * (semi_perimeter - length_a) * (semi_perimeter - length_b)
                  * (semi_perimeter - length_c)) ** 0.5

        return result


p1 = Point(3, 7)
p2 = Point(4, 8)
line1 = Line(p1, p2)
p3 = Point(5, 20)
triangle_1 = Triangle(p1, p2, p3)
print(triangle_1.square())
