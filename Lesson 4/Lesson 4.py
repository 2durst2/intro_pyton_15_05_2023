
# Задача 1: Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів,
# які містять дві голосні літери підряд.

# Задача реализована только для русского языка.

vowels_str = str("аеёиоуыэюя")
user_str = input("Введите строку: ").lower()
user_list = user_str.split()
quantity_words = 0

for word in user_list:

    if len(word) < 2:
        continue

    previos_symbol = ""
    tuple_symbol = tuple(word)

    for symbol in tuple_symbol:

        if vowels_str.find(symbol) > -1 and previos_symbol == symbol:
            quantity_words += 1
            break
        else:
            previos_symbol = symbol

print(f"Количество слов с двумя гласными подряд: {quantity_words}")

# Задача 2: Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів
# і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,
# "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між
# мінімальною і максимальною ціною. Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

dict_shop = {"cito": 47.999,
             "BB_studio": None,
             "momo": 49.999,
             "main-service": 37.245,
             "buy.now": 38.324,
             "x-store": 37.166,
             "the_partner": 38.988,
             "store": 37.720,
             "rozetka": 38.003}

user_lower = input("Введите минимальцую цену: ").strip(" +-")
try:
    lower_price = float(user_lower)
except ValueError:
    print("Некорректно введена минимальная цена! Присвоено значение по умолчанию: 0")
    lower_price = 0

user_upper = input("Введите максимальную цену: ").strip(" +-")
try:
    upper_price = float(user_upper)
except ValueError:
    print("Некорректно введена максимальная цена! Присвоено значение по умолчанию: 0")
    upper_price = 0

str_shop = ""

for key, value in dict_shop.items():

    try:
        value_float = float(value)
    except ValueError:
        continue
    # Оказалось, что может быть ещё и TypeError, поэтму поставил общее исключение
    except Exception as ex:
        continue

    # По ДЗ не ясно границы цен включать или исключать, сделал включая
    if lower_price <= value_float <= upper_price:
        str_shop += f"{key}, "

str_shop = str_shop.rstrip(" ,") if len(str_shop) > 0 else "Нет подходящих магазинов"
print(f"Список магазинов: {str_shop}")
