# Задача 1: Напишіть гру "Rock paper scissors" https://en.wikipedia.org/wiki/Rock_paper_scissors
# # Грають гравець і компʼютер. Вdід даних гравцем - через input()
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг.
# Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду (DRY, KISS, YAGNI).

import random


def get_available_values():
    """
    Возвращает dict в котором хранится информация о том какой объект (key) побеждает какой (value)
    Returns:
        (dict): dict в котором key(str), value(tuple из str)
    """
    # В значение dict специально поместил тип tuple, чтобы в дальнейшем можно было без изменения функционала расширять
    # список того, что побеждает ключ. Т.е сейчас камень победит ножницы, а можно добавить в значение ящерицу и
    # камень будет побеждать ножницы и ящерицу
    values = {
        "камень": ("ножницы"),
        "ножницы": ("бумага"),
        "бумага": ("камень")
    }

    return values


def get_user_intput(available_values):
    """
    Возвращает выбранный пользователем ключ из доступных ключей dict
    Args:
        available_values (dict): доступные значения
    Returns:
        (str): выбранное значение
    """
    # Специально преобразовал в лист, так как available_values.keys выводило слово dict_keys,
    # а потом доступные значения, что не очень красиво.
    str_massage = f"Введите одно из доступных доступных значений {list(available_values)}: "
    while True:

        user_intput = input(str_massage).lower().strip(" +-")
        value = available_values.get(user_intput, None)
        if value is None:

            print("Введены некорректные данные!")

        else:

            break

    result = user_intput

    return result


def user_input(available_values, its_user=True):
    """
    Возвращает выбранный ключ из доступных от пользователя или компьютера
    Args:
        available_values (dist): доступные значения
        its_user (bool): признак того, что данные вводит пользователь(True) или компьютер(False)
    Returns:
        (str): выбранный ключ
    """
    if its_user:

        str = "Пользователь"
        result = get_user_intput(available_values)

    else:

        str = "Компрьютер"
        result = random.choice(list(available_values))

    print(f"{str} выбрал: {result}")

    return result


def get_winner(user_value, pc_value, available_values):
    """
    Определяет победителя и выводит его на экран
    Args:
         user_value (str): выбранное значение от пользователя
         pc_value (str): выбранное значение от комьютера
         available_values (dict): dict с описанием какой ключ, побеждает какое значение
    """
    if user_value == pc_value:

        result = "Ничья"

    elif pc_value in available_values.get(user_value):

        result = "Победил пользователь"

    else:

        result = "Победил компьютер"

    print(result)


def play_again():
    """
    Возвращает признак того, что пользователь хочет сыграть еще раз.
    Returns:
        (bool): пользователь хочет играть
    """
    while True:

        user_answer = input("Хотите сыграеть ещё раз? Введите Да/Нет: ").lower().strip(" +-")

        if user_answer == "да":

            return True

        elif user_answer == "нет":

            return False


def start_game():
    """"
    Основная функция в которой происходит игра
    """
    available_values = get_available_values()
    user_play = True

    while user_play:

        user_value = user_input(available_values)
        pc_value = user_input(available_values, False)
        get_winner(user_value, pc_value, available_values)
        user_play = play_again()


start_game()
