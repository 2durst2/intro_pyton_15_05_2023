
# Задача 1: Напишіть код, який зформує строку, яка містить певну інформацію про символ за його номером у слові.
# Наприклад "The [номер символу] symbol in '[тут слово]' is '[символ з відповідним порядковим номером в слові]'".
# Слово та номер символу отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in 'Python' is 't' ".

user_str = input("Введите любую строку: ").strip(" ")
do_loop = True

while do_loop:
    user_number_str = input("Укажите номер интересующего символа: ").strip(" +-")
    if not user_number_str.isdigit():
        print("Номер символа должен быть целым числом!")
    else:
        index = int(user_number_str) - 1
        str_len = len(user_str)
        if index < 0 or index >= str_len:
            print(f"Номер интересующего символа должен быть от 1 до {str_len}")
        else:
            letter = user_str[index]
            print(f"{index+1}й символ в строке \"{user_str}\" - \"{letter}\"")
            do_loop = False
            break
    while True:
        user_answer = input("Повторить попытку? Введите: Да/Нет ").upper().strip()
        if user_answer == "ДА":
            break
        elif user_answer == "НЕТ":
            do_loop = False
            break

# Задача 2: Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою).
# Напишіть код, який визначить кількість слів, в цих даних.

user_str2 = input("Введите строку: ").strip(" ")
list2 = user_str2.split()
quantity_words = 0
for element in list2:

    # нет слов из 1й буквы
    if len(element) > 1:
        quantity_words += 1

print(f"количество слов в строке = {quantity_words}")

# Задача 3: Існує ліст з різними даними,
# наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ["1", "2", 3, True, "False", 5.1, "6", 7, 8, "Python", 9, 0, "Lorem Ipsum"]
lst2 = list()

for element in lst1:

    if type(element) == int or type(element) == float:
        lst2.append(element)

print(lst2)
