
age_str = input("Вкажіть свій вік: ").strip(" ")

if not age_str.isdigit():
    print("Введено некоректні дані!")
elif age_str.find("7") > -1:
    print("Вам пощастить!")
elif int(age_str) < 6:
    print("Де твої батьки?")
elif int(age_str) < 16:
    print("Це фільм для дорослих!")
elif int(age_str) > 65:
    print("Покажіть пенсійне посвідчення!")
else:
    print("А білетів вже немає!")

