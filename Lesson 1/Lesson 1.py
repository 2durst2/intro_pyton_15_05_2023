
# Задача 1: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної
# взаємодії (+, -, *, / и тд.) для цих чисел.

first = 10
second = 30

result = first + second
print(result)

result = first - second
print(result)

result = first * second
print(result)

result = first / second
print(result)

result = first // second
print(result)

result = first ** second
print(result)

result = first % second
print(result)

# Задача 2: Створіть змінну і почергово запишіть в неї результат порівняння (<, > , ==, !=) чисел з
# завдання 1. Виведіть на екран результат кожного порівняння.

result_bool = first < second
print(result_bool)

result_bool = first > second
print(result_bool)

result_bool = first == second
print(result_bool)

result_bool = first != second
print(result_bool)

# Задача 3: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world". Виведіть на екран.

str1 = "Hello "
str2 = "world"

result_str = str1 + str2
print(result_str)

# Вопрос 1: Что Вы считаете результатом данного курса?
# Понимание истересно ли мне будет развиваться в данном направлении дальше (учить именно этот язык).
# Наличие начальных знаний в новом для меня языке программирования.
# Наличие серитификата будет являться приятным бонусом☺.

# Вопрос 2: Как Вы собираетесь использовать этот результат?
# Если методика написания программ на Pyton понравиться - буду дальше развиваться в этом ключе (курсы + саморазвитие).
# На текущей работе есть проект который для внедрения и сопровождения требует знания Pyton.

# Вопрос 3: Чем Вы готовы пожертвовать для достижения этого результата?
# Временем и деньгами.
