
# Задача 1: Доопрацюйте гру з заняття наступним чином:
# Додайте вибір слова з будь-якого джерела на ваш вибір
# Додайте лічильник спроб вгадати слово і вихід з циклу по переповненню лічильника
# Додайте більш інформативні повідомлення (список можливих букв для введення, повідомлення про невірне введення,
# повідомлення про кількість спроб що залишилися і тд)
# Опційно. Додайте можливість повторювати гру. Запитати в гравця чи хоче він повторити гру Yes/No і повторювати якщо
# він введе Yes, завершити якщо введе No

import random

word_set = ("Привет", "Терминатор", "Кувалда", "Программист", "Сон")

# Переделал переменные entered_letters и available_letters на list, чтобы текст сообщений был однотипным,
# так как выводится одного типа коллеции.
available_letters = list("абвгдеёжзийклмнопрстуфхцчшщъыьэюя")
# объявим строку заранее, чтобы в цикле не формировать одну и туже n кол-во раз.
str_available_letters = f"Введен недопустимый символ. Перечень доступных букв для ввода: {available_letters}"

# Не знаю что является правильным тоном в python, создание Mutable переменных в цикле или вне цикла с последующей
# очисткой перед использованием. Пошел по 2му пути.
guessed_letters = []
entered_letters = list()
user_play = True

while user_play:

    guessed_letters.clear()
    entered_letters.clear()

    word = random.choice(word_set).lower()
    len_word = len(word)
    guess_result = "*" * len_word

    number_attempts = 0
    additionally_attempts = 3
    max_attempts = len_word + additionally_attempts
    # print(word)
    while guess_result != word:

        if number_attempts >= max_attempts:

            print(f"Конец игры. Вам не удалось отгадать слово: {word}")
            break

        print(f"Угадайте слово из {len_word} букв: {guess_result}")

        while True:
            user_letter = input("Введите букву: ").lower()
            if len(user_letter) != 1:
                print("Необходимо вводить 1 букву.")
                continue
            elif user_letter not in available_letters:
                print(str_available_letters)
                continue
            elif user_letter in entered_letters:
                print(f"Данная буква уже вводилась! Перечень уже введенных букв: {entered_letters}")
                continue

            entered_letters.append(user_letter)
            break

        if user_letter in word:
            guessed_letters.append(user_letter)
            guess_result = ''.join(['*' if char not in guessed_letters else char for char in word])
        else:
            print("Неверно! Попробуйте ещё!")

        number_attempts += 1
        print(f"Осталось: {max_attempts - number_attempts} попыток.")

    if guess_result == word:
        print(f"Поздравляем, Вы отгадали слово: {word}!")

    while True:
        user_answer = input("Хотите сыграеть ещё раз? Введите Да/Нет: ").lower().strip(" +-")
        if user_answer == "да":
            break
        elif user_answer == "нет":
            user_play = False
            break
