
# Задача 1:
# а. Реалізуйте перевірку даних на те що вершини є Point за допомогою property
# б. Реалізуйте ітератор по вершинам трикутника


class Point:
    x_coord = 0
    y_coord = 0

    def __init__(self, x, y):

        self.x_coord = x
        self.y_coord = y

    def __str__(self):

        return f'Point {self.x_coord} {self.y_coord}'

    def __getitem__(self, item):

        # print(f'__getitem__ {item}')
        if item == 0:
            return self.x_coord
        elif item == 1:
            return self.y_coord
        else:
            raise TypeError

    def __setitem__(self, item, value):
        # print(f'__setitem__ {item}, {value}')
        if item == 0:
            self.x_coord = value
        elif item == 1:
            self.y_coord = value
        else:
            raise TypeError

    def __eq__(self, other):
        # check type Point
        return self.x_coord == other.x_coord and self.y_coord == other.y_coord

    def __setattr__(self, key, value):

        type_value = type(value)
        if type_value != int and type_value != float:
            raise TypeError(f"for key {key} value must bee int or float")

        self.__dict__[key] = value


class Line:

    begin_point = None
    end_point = None

    def __init__(self, begin, end):

        self.begin_point = begin
        self.end_point = end

    def __str__(self):
        return f'Line from [{self.begin_point}] to [{self.end_point}]'

    def length(self):
        k1 = self.begin_point.x_coord - self.end_point.x_coord
        k2 = self.begin_point.y_coord - self.end_point.y_coord

        return (k1 ** 2 + k2 ** 2) ** 0.5

    def __len__(self):
        """ len(obj) """
        return 2

    def __contains__(self, item):
        """ a in b """
        print('__contains__', item)
        return self.begin_point == item or self.end_point == item

    def __setattr__(self, key, value):

        list_key = ["begin_point", "end_point"]
        if key in list_key and not isinstance(value, Point):
            raise TypeError(f"for key: {key} value must bee class Point")

        self.__dict__[key] = value


class Triangle:

    _point_a = None
    _point_b = None
    _point_c = None
    _current = 0

    @staticmethod
    def _check_type_point(key, value):

        if not isinstance(value, Point):
            raise TypeError(f"for key: {key} value must bee class Point")

    @property
    def point_a(self):

        return self._point_a

    @point_a.setter
    def point_a(self, value):

        self._check_type_point("point_a", value)
        self._point_a = value

    @property
    def point_b(self):

        return self._point_b

    @point_b.setter
    def point_b(self, value):

        self._check_type_point("point_b", value)
        self._point_b = value

    @property
    def point_c(self):

        return self._point_c

    @point_c.setter
    def point_c(self, value):

        self._check_type_point("point_c", value)
        self._point_c = value

    def __init__(self, new_a, new_b, new_c):

        self.point_a = new_a
        self.point_b = new_b
        self.point_c = new_c

    def __iter__(self):

        self._current = 0
        return self

    def __next__(self):

        list_point = [self.point_a, self.point_b, self.point_c]
        if self._current >= len(list_point):

            raise StopIteration

        result = list_point[self._current]
        self._current += 1
        return result

    def square(self):

        line_a = Line(self.point_a, self.point_b)
        line_b = Line(self.point_b, self.point_c)
        line_c = Line(self.point_a, self.point_c)

        length_a = line_a.length()
        length_b = line_b.length()
        length_c = line_c.length()

        semi_perimeter = 0.5 * (length_a + length_b + length_c)
        result = (semi_perimeter * (semi_perimeter - length_a) * (semi_perimeter - length_b)
                  * (semi_perimeter - length_c)) ** 0.5

        return result


p1 = Point(3, 7)
p2 = Point(4, 8)
line1 = Line(p1, p2)
p3 = Point(5, 20)
triangle_1 = Triangle(p1, p2, p3)
print(triangle_1.square())
for t_value in triangle_1:

    print(t_value)
